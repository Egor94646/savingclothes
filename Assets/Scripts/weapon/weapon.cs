using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weapon : types_of_weapons
{
    private float fireRate = 21;
    private float damage = 1;
    private float range = 30;
   
    [SerializeField] private GameObject muzzleFlash;
    [SerializeField] private AudioClip shotsFX;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private Camera cam;
    [SerializeField] private Transform bulletSpawn;

    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            ShootRaycast(muzzleFlash, bulletSpawn, cam, range, damage, 1);
        }
    }
}
