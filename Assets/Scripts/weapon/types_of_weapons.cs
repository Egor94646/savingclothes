using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class types_of_weapons : MonoBehaviour
{
    private int _lvl_weapon;
    public void ShootRaycast(GameObject muzzleFlash, Transform bulletSpawn, Camera cam, float range,float damage, int lvl)
    {
        _lvl_weapon = lvl;
        Instantiate(muzzleFlash, bulletSpawn.position, bulletSpawn.rotation);

        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range*_lvl_weapon))
        {
            if(hit.collider.gameObject.tag == "mole")
            {
                hit.collider.gameObject.GetComponent<mole_test>().Hp -= damage * _lvl_weapon;
            }            
        }
    }
}
