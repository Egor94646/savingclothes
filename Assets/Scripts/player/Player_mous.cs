using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_mous : MonoBehaviour
{
    private float mouse_x, mouse_y;
    private float mouse_x_current, mouse_y_current;
    private float Current_velositX, Current_velositY;

    [SerializeField] private float mouse_speed = 3.5f;
    [SerializeField] private Camera player_camera;
    [SerializeField] private float SmoothTime = 0.1f;


    void Start()
    {
        player_camera = Camera.main;
        Cursor.visible = false;
    }
    void FixedUpdate()
    {
        MouseMove();
    }

    void MouseMove()
    {
        mouse_x += Input.GetAxis("Mouse X") * mouse_speed;
        mouse_y += Input.GetAxis("Mouse Y") * mouse_speed;
        mouse_y = Mathf.Clamp(mouse_y, -75, 90);

        mouse_x_current = Mathf.SmoothDamp(mouse_x_current, mouse_x, ref Current_velositX, SmoothTime);
        mouse_y_current = Mathf.SmoothDamp(mouse_y_current, mouse_y, ref Current_velositY, SmoothTime);

        player_camera.transform.rotation = Quaternion.Euler(-mouse_y_current, mouse_x_current, 0f);
        transform.rotation = Quaternion.Euler(0f, mouse_x_current, 0f);
    }
}
