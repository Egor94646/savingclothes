using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_down : MonoBehaviour
{
    void FixedUpdate()
    {
       if(Input.GetKey(KeyCode.LeftShift))
       {
            gameObject.transform.localScale = new Vector3(1f, 0.7f, 1f);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}
