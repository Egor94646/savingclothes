using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_move : MonoBehaviour
{
    private float ver, hor, jump;
    [SerializeField] private bool isGround;
    [SerializeField] private bool run_bool = false;
    [SerializeField] private float JumpSpeed = 200f, run_speed = 17f, currentSpeed, standartSpeed = 8f;
    [SerializeField] public Slider slider;
    public float staminaValue = 75f;

    void FixedUpdate()
    {
        StaminaVoid();
        staminaValue = Mathf.Clamp(staminaValue, 0, 100);
        slider.GetComponent<Slider>().value = staminaValue;
        currentSpeed = currentSpeed;
        ver = Input.GetAxis("Vertical") * Time.deltaTime * currentSpeed;
        hor = Input.GetAxis("Horizontal") * Time.deltaTime * currentSpeed;
        jump = Input.GetAxis("Jump") * Time.deltaTime * JumpSpeed;

        if (isGround)
        {
            GetComponent<Rigidbody>().AddForce(transform.up * jump, ForceMode.Impulse);
        }

        transform.Translate(new Vector3(hor, 0, ver));
    }

    void OnCollisionStay(Collision coll)
    {
        if (coll.gameObject.tag == "ground")
        {
            isGround = true;
        }
    }

    void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.tag == "ground")
        {
                isGround = false;
        }
    }

    public void StaminaVoid()
    {
        if(Input.GetKey(KeyCode.R)&& staminaValue > 0f)
        {
            currentSpeed = run_speed;
            staminaValue -= 15 * Time.deltaTime;
        }
        if(!(Input.GetKey(KeyCode.R) && staminaValue > 0f))
        {
            currentSpeed = standartSpeed;
            staminaValue += 15 * Time.deltaTime;
        }
    }
}



