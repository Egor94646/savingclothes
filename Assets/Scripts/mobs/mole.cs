using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class mole : MonoBehaviour
{
    public float Hp { get; set; }
    private float _speedEat;
    private int _money;
    private float _speedMove;

    private float _maxPosX;
    private float _maxPosZ;

    private float _minPosX;
    private float _minPosZ;

    private float _posX;
    private float _posZ;

    private const float _maxDistance = 1f;
   /* protected void EatClothes(GameObject mole, GameObject clothes)
    {
        //������� ������ � ������� ����� ������, ����� ����� ����������� ������� ����� ���������
        if(Vector3.Distance(mole.gameObject.transform.position,clothes.gameObject.transform.position)> _maxDistance)
        {
            DestroyObject(clothes.gameObject);
        }
    }*/
    protected void MoveMole()
    {
        this.gameObject.transform.Translate(new Vector3(_speedMove * Time.deltaTime, 0, 0));
    }

    protected void BindingToClothes(GameObject mole, GameObject clothes)
    {
        _maxPosX = clothes.gameObject.transform.lossyScale.x / 2 + clothes.gameObject.transform.position.x + 2.5f;
        _maxPosZ = clothes.gameObject.transform.lossyScale.z / 2 + clothes.gameObject.transform.position.z + 2.5f;

        _minPosX = -(clothes.gameObject.transform.lossyScale.x / 2) + clothes.gameObject.transform.position.x + 2.5f;
        _minPosZ = -(clothes.gameObject.transform.localScale.z / 2) + clothes.gameObject.transform.position.z + 2.5f;

        
        if (mole.gameObject.transform.position.x > _maxPosX)
        {
            mole.gameObject.transform.position = new Vector3(_maxPosX, mole.gameObject.transform.position.y, mole.gameObject.transform.position.z);
        }
        if (mole.gameObject.transform.position.x < _minPosX)
        {
            mole.gameObject.transform.position = new Vector3(_minPosX, mole.gameObject.transform.position.y, mole.gameObject.transform.position.z);
        }

        if (mole.gameObject.transform.position.z > _maxPosZ)
        {
            mole.gameObject.transform.position = new Vector3(mole.gameObject.transform.position.x, mole.gameObject.transform.position.y, _maxPosZ);
        }
        if (mole.gameObject.transform.position.z < _minPosZ)
        {

            mole.gameObject.transform.position = new Vector3(mole.gameObject.transform.position.z, mole.gameObject.transform.position.y, _minPosZ);
        }
    }

    protected void init_specificationsMole()
    {
        Hp = 3;
        _speedEat = 3;
        _money = 50;
        _speedMove = 0.1f;
    }

    protected void init_specificationsMole(float hp1, float speedEat, int money_mole, float speedMove)
    {
        Hp = hp1;
        _speedEat = speedEat;
        _money = money_mole;
        _speedMove = speedMove;
    }

    protected void Change_mole_state()
    {
        if (Hp < 1)
        {
            gameGUI.s_money += _money;
            DestroyObject(this.gameObject);
        }
    }
}
