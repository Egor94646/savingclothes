using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_mobs : MonoBehaviour
{
    [SerializeField] private GameObject mobs;
    private int i = 3;
    private float _scale_spawner;
    private float _scale_mob;

    private void Start()
    {
        _scale_spawner = gameObject.GetComponent<BoxCollider>().size.x * gameObject.GetComponent<BoxCollider>().size.z;
        _scale_mob = mobs.gameObject.GetComponent<BoxCollider>().size.x * mobs.gameObject.GetComponent<BoxCollider>().size.z;
        i = (int)(_scale_spawner / _scale_mob);
        while (i > 1)
        {
            SpawnOneEnemy();
            i--;
        }
    }

    private void SpawnOneEnemy()
    {
        float posX = Random.Range(-((gameObject.GetComponent<BoxCollider>().size.x) / 2), ((gameObject.GetComponent<BoxCollider>().size.x) / 2));
        float posY = Random.Range(-((gameObject.GetComponent<BoxCollider>().size.y) / 2), ((gameObject.GetComponent<BoxCollider>().size.y) / 2));
        float posZ = Random.Range(-((gameObject.GetComponent<BoxCollider>().size.z) / 2), ((gameObject.GetComponent<BoxCollider>().size.z) / 2));

        Instantiate(mobs, new Vector3(transform.position.x + posX, transform.position.y + posY, transform.position.z + posY), Quaternion.identity);
    }
}
