using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class GUIScripts : MonoBehaviour
{
    [SerializeField] private GameObject mainCanvas;
    [SerializeField] private GameObject settingsCanvas;
    [SerializeField] private GameObject levelsCanvas;
    [SerializeField] private GameObject magazineCanvas;

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private Text _text_cash;
    private float _musicVolume = 1f;

    private bool _isFullScreen = true;

    [SerializeField] private Slider _slader_volume;

    private int _index_canvas = 0;

    private void FixedUpdate()
    {
        _text_cash.text = "$" + gameGUI.s_money.ToString();  
    }

    public void SwitchCanvas(int i)
    {
        switch (i)
        {
            case -1:
                Application.Quit();
                break;
            case 0:
                mainCanvas.SetActive(true);
                settingsCanvas.SetActive(false);
                levelsCanvas.SetActive(false);
                magazineCanvas.SetActive(false);
                break;
            case 1:
                mainCanvas.SetActive(false);
                settingsCanvas.SetActive(true);
                levelsCanvas.SetActive(false);
                magazineCanvas.SetActive(false);
                break;
            case 2:
                mainCanvas.SetActive(false);
                settingsCanvas.SetActive(false);
                levelsCanvas.SetActive(true);
                magazineCanvas.SetActive(false);
                break;

            case 3:
                mainCanvas.SetActive(false);
                settingsCanvas.SetActive(false);
                levelsCanvas.SetActive(false);
                magazineCanvas.SetActive(true);
                break;
        }
    }
 
    public void FullScreenToggle()
    {
        _isFullScreen = !_isFullScreen;
        Screen.fullScreen = _isFullScreen;
    }

    public void AudioVolume()
    {
        _musicVolume = _slader_volume.value;
        _audioSource.volume = _musicVolume;
    }

    public void Quality(int q)
    {
        QualitySettings.SetQualityLevel(q);
    }

    public void level_controller(string name_lvl)
    {
        SceneManager.LoadScene(name_lvl);
    }
}