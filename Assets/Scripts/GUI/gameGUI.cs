using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameGUI : MonoBehaviour
{
    public static int s_money = 0;
    [SerializeField] private Text _text_cash;

    private void Update()
    {
        _text_cash.text = "$" + s_money.ToString();
        if (Input.GetKeyDown(KeyCode.F1))
        {
            gameMenu();
        }
    }
    private void gameMenu()
    {
        SceneManager.LoadScene("Menu");
        Cursor.visible = true;
    }
}
