using UnityEngine;

public abstract class MonsterBase : MonoBehaviour, IMnster
{
    public int hp;

    public void TakeDamage()
    {
        hp--;
    }
}
